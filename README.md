This C project demonstrates a simple environment for building a small
application with GNU Make.

The Makefile controls which files are gathered for compilation and inclusion. It
is an attempt to follow best practices when using GNU Make.

